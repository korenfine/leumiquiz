const axios = require('axios')
const xlsx = require('xlsx')

// global variables
const citiesList = ['Jerusalem', 'New York', 'Dubai', 'Lisbon', 'Oslo', 'Paris', 'Berlin', 'Athens', 'Seoul', 'Singapore']
const weekday = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']

// api connection data
const APPID = 'fff9f900987a05902f07d757aba5540f'
const apiLink = `https://api.openweathermap.org/data/2.5/forecast?appid=${APPID}`

;(async () => {
    try {
        // build all axios request
        const promiseData = citiesList.map(city => axios.get(`${apiLink}&q=${city}`))

        // run all axios
        const response = (await Promise.all(promiseData)).map(res => res.data)

        // get forecast list
        const forecastList = response.reduce(getForecastList, [])

        // run over list and change his inside data
        forecastList.forEach(forecast => {
            // change array to string
            forecast['cities with rain'] = forecast['cities with rain'].join(', ')

            // remove json keys
            delete forecast.highestTemp
            delete forecast.lowestTemp
        })

        // build csv
        buildCsv(forecastList, 'forecast')

        console.log('CSV file created')
    } catch (err) {
        throw Error(err)
    }

})()


/**
 * Build csv
 * @param a {array} previous array.
 * @param b {object} array next element.
 * @return array.
 */
const getForecastList = (a, b) => {
    b.list.forEach(elem => {
        // get date and his text
        const dayText = weekday[(new Date(elem.dt_txt)).getDay()]

        // get data element from day
        const forecast = a.find(d => d.DAY === dayText)

        // if not exist add new element to forecast list
        if (!forecast) {
            a.push({
                DAY: dayText,
                'city with highest temp': b.city.name,
                highestTemp: elem.main.temp_max,
                'city with lowest temp': b.city.name,
                lowestTemp: elem.main.temp_min,
                'cities with rain': elem.rain ? [b.city.name] : []
            })

            return
        }

        // update cities and temperatures in forecast list
        Object.assign(forecast, {
            'city with highest temp': (forecast.highestTemp < elem.main.temp_max) // highest temperature city name
                ? b.city.name
                : forecast['city with highest temp'],
            highestTemp: (forecast.highestTemp < elem.main.temp_max) // highest temperature
                ? elem.main.temp_max
                : forecast.highestTemp,
            'city with lowest temp': (forecast.lowestTemp > elem.main.temp_min) // lowest temperature city name
                ? b.city.name
                : forecast['city with lowest temp'],
            lowestTemp: (forecast.lowestTemp > elem.main.temp_min) // lowest temperature
                ? elem.main.temp_min
                : forecast.lowestTemp
        })

        // add cities with rain if not exist
        if (elem.rain && !forecast['cities with rain'].includes(b.city.name)) {
            forecast['cities with rain'].push(b.city.name)
        }
    })

    return a
}


/**
 * Build csv
 * @param list {object} list of forecast data.
 * @param fileName {string} file name.
 * @return nothing.
 */
const buildCsv = (list = {}, fileName = 'file') => {
    // build json for csv
    const ws = xlsx.utils.json_to_sheet(list)

    // create new sheet and add data
    const wb = xlsx.utils.book_new()
    xlsx.utils.book_append_sheet(wb, ws)

    // write to file
    xlsx.writeFile(wb, `${fileName}.csv`)
}

